'use strict';

    window.onload = preloadWindow();

    function createPost(post){
        let post_content = `
            <!-- single post start -->
            <div id = "${post.id}">
                <!-- image block start -->
                <div>
                    <img class="d-block w-100 post-image" src="images/${post.img}" alt="Post image">
                </div>
                <!-- image block end -->
                <div class="px-4 py-3">

                    <!-- post reactions block start -->
                    <div class="d-flex justify-content-around">
                        <span class="h1 mx-2 muted" id ="like.${post.id}" onclick="changeLike(this.id);">
                          <i class="far fa-heart"></i>
                        </span>
                        <span class="h1 mx-2 muted" id ="comment.${post.id}" onclick="changeComment(this.id);"">
                          <i class="far fa-comment"></i>
                        </span>
                        <span class="mx-auto"></span>
                        <span class="h1 mx-2 muted" id ="bookmark.${post.id}" onclick="changeBookmark(this.id);"">
                          <i class="far fa-bookmark"></i>
                        </span>
                    </div>
                    <!-- post reactions block end -->
                    <div>
                        <p> ${post.description} </p>
                    </div>
                    <hr>
                    <!-- comments section start -->
                    <div class="py-2 pl-3 comments">
                      <div>
                        <a href="#" class="muted">someusername</a>                    
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim
                        voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
                      </div>
                        <button type = "button" id = "getComments.${post.id}" onclick="getComments(this.id)">Show Comments</button>
                        <form id="comment-form" class="commentForm" name="comment-form" enctype="form-data" action="/createComment" method="post">       
                            <p><b>Add your comment</b></p>
                            <input type="hidden" name="user_id" value="1" />
                            <input type="hidden" name="post_id" value="${post.id}"/>
                            <p class = "text-area"><textarea name="comment" class = "comment"></textarea></p>
                            <button type="button" id="save-comment" onclick="addComment(this.form)">Add</button>
                        </form>
                    <!-- comments section end -->
                </div>

            </div>
            <!-- single post end -->`;
        let element = document.createElement(`div`);
        element.innerHTML += post_content;
        element.classList.add("card", "my-3");
        return element;

    }

    class Post{
        constructor(id, img, description) {
            this.id = id;
            this.img = img;
            this.description = description;
        }
    }
    class Comment{
        constructor(user_id, post_id, comment) {
            this.user_id = user_id;
            this.post_id = post_id;
            this.comment = comment;
        }
    }
    async function getPosts() {
        let url = "http://localhost:8080/publications";
        let response = await fetch(url);
        return await response.json();
    }

    async function preloadWindow() {
        let posts = await getPosts();
        for(let i = 0; i<posts.length; i++){
            let post = new Post(posts[i].id, posts[i].image, posts[i].description);
            let elem = createPost(post);
            document.getElementById("posts-container").appendChild(elem);
        }
    }
    function changeLike(id){
        let heart = document.getElementById(id).children[0];
        if(heart.classList.contains("text-danger")){
            heart.classList.remove("text-danger","fas");
        }else{
            heart.classList.add("text-danger","fas");
        }
    }

    function changeBookmark(id){
        let saveIcon = document.getElementById(id).children[0];
        if(saveIcon.classList.contains("fas")){
            saveIcon.classList.remove("fas");
        }else{
            saveIcon.classList.add("fas");
        }
    }

    function changeComment(id){
        let postId = id.replace('comment.','');
        let commentForm = document.getElementById(postId).getElementsByTagName('form')[0];
        if(commentForm.classList.contains("commentForm")){
            commentForm.classList.remove("commentForm")
        }
        else{
            commentForm.classList.add("commentForm")
        }
    }

    function createCommentElement(comment){
        let content = '<a href="#" class="muted">' + comment.user_id + '</a>' + '<p>'+ comment.comment + '</p>';
        let element = document.createElement('div');
        element.innerHTML = content;
        return element;
    }

    function addComment(form){
        let data = new FormData(form);
        let post_id = data.get("post_id").toString();

        let nc = new Comment(data.get("user_id"), data.get("post_id"), data.get("comment"));
        let ncElem = createCommentElement(nc);

        document.getElementById(post_id).getElementsByClassName("comments")[0].children[0].append(ncElem);

        fetch("http://localhost:8080/createComment", {
            method: "POST",
            body: data
        }).then(r => r.json()).then(data => {
            console.log(data);
        });
    }

    async function getComments(id) {
        let postId = id.replace("getComments.", "");
        let response = await fetch("http://localhost:8080/getComments/"+postId);
        if(response.ok) {
            let comments = await response.json();
            document.getElementById(postId).getElementsByClassName("comments")[0].children[0].innerHTML = "";
            for(let i = 0; i<comments.length; i++){
                let nc = new Comment("user", postId, comments[i].comment);
                let ncElem = createCommentElement(nc);
                document.getElementById(postId).getElementsByClassName("comments")[0].children[0].append(ncElem);
            }
        } else {
        }
    }
    function showModalWin() {

        var darkLayer = document.createElement('div'); // слой затемнения
        darkLayer.id = 'shadow'; // id чтобы подхватить стиль
        document.body.appendChild(darkLayer); // включаем затемнение

        var modalWin = document.getElementById('modal_window'); // находим наше "окно"
        modalWin.hidden = false;
        modalWin.style.display = 'block'; // "включаем" его

        darkLayer.onclick = function () {  // при клике на слой затемнения все исчезнет
            darkLayer.parentNode.removeChild(darkLayer); // удаляем затемнение
            modalWin.style.display = 'none'; // делаем окно невидимым
            return false;
        };


    }
    const saveButton = document.getElementById("save-publication");
    saveButton.addEventListener("click", function() {
        const publicationForm = document.getElementById("form");
        let data = new FormData(publicationForm);
        fetch("http://localhost:8080/publications", {
            method: 'POST',
            body: data
        }).then(r => r.json()).then(data => {window.location.href = "http://localhost:8080/"});
    });


