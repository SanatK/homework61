
package kg.attractor.microgram.dto;

import kg.attractor.microgram.model.*;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class SubscribesDTO {

    public static SubscribesDTO from(Subscribes subscription) {
        return builder()
                .id(subscription.getId())
                .subscriberId(subscription.getSubscriberId())
                .userId(subscription.getUserId())
                .subscriptionTime(subscription.getSubscriptionTime())
                .build();
    }

    private String id;
    private String subscriberId;
    private String userId;
    private LocalDateTime subscriptionTime;
}