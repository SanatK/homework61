
package kg.attractor.microgram.dto;

import kg.attractor.microgram.model.Comment;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CommentDTO {

    public static CommentDTO from(Comment comment) {
        return builder()
                .id(comment.getId())
                .userId(comment.getUserId())
                .publicationId(comment.getPublicationId())
                .comment(comment.getComment())
                .commentTime(comment.getCommentTime())
                .build();
    }

    private String id;
    private String userId;
    private String publicationId;
    private String comment;
    private LocalDateTime commentTime;
}
