
package kg.attractor.microgram.dto;

import kg.attractor.microgram.model.Publication;
import kg.attractor.microgram.model.Publication;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PublicationDTO {

    public static PublicationDTO from(Publication publication) {

        return builder()
                .id(publication.getId())
                .description(publication.getDescription())
                .publicationTime(publication.getPublicationTime())
                .image(publication.getImage())
                .build();
    }
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String description;
    private LocalDateTime publicationTime;
    private String image;

}
