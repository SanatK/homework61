package kg.attractor.microgram.util;

import kg.attractor.microgram.model.Publication;
import kg.attractor.microgram.repository.PublicationRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class PreloadDatabaseWithData {
    public final PublicationRepository publicationRepository;
    public PreloadDatabaseWithData (PublicationRepository publicationRepository){
        this.publicationRepository = publicationRepository;
    }
    @Bean
    public void fillData(){
        publicationRepository.deleteAll();
        List<Publication> pub = new ArrayList<>();
        pub.add(new Publication("New Post 1", "image1.jpg"));
        pub.add(new Publication("New Post 2", "image2.jpg"));
        pub.add(new Publication("New Post 3", "image3.jpg"));
        publicationRepository.saveAll(pub);
    }
}
