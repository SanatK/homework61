package kg.attractor.microgram.controller;

import kg.attractor.microgram.model.Comment;
import kg.attractor.microgram.repository.CommentRepository;
import kg.attractor.microgram.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
public class CommentController {
    @Autowired
    CommentService commentService;
    CommentRepository commentRepository;

    @RequestMapping(value = "/createComment", method = RequestMethod.POST)
    public final String addComment(@RequestParam("user_id") String user_id,
                                   @RequestParam("post_id") String post_id,
                                   @RequestParam("comment") String comment) throws IOException{
       return commentService.createComment(user_id, post_id, comment);
    }

}
