package kg.attractor.microgram.controller;

import kg.attractor.microgram.dto.CommentDTO;
import kg.attractor.microgram.dto.LikeDTO;
import kg.attractor.microgram.dto.SubscribesDTO;
import kg.attractor.microgram.model.Publication;
import kg.attractor.microgram.model.User;
import kg.attractor.microgram.repository.PublicationRepository;
import kg.attractor.microgram.service.PublicationService;
import kg.attractor.microgram.service.SubscribesService;
import kg.attractor.microgram.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class PublicationController {
    private  PublicationService publicationService;
    private  UserService userService;
    private  SubscribesService subscriptionService;
    private  PublicationRepository publicationRepository;
    public PublicationController(PublicationService publicationService, UserService userService, SubscribesService subscriptionService, PublicationRepository publicationRepository){
        this.publicationService = publicationService;
        this.userService = userService;
        this.subscriptionService = subscriptionService;
        this.publicationRepository = publicationRepository;
    }
    @GetMapping
    public String root(Model model) {
        model.addAttribute("publications", publicationRepository.findAll());
        return "publications";
    }

    @GetMapping("/publications")
    @ResponseBody
    public Iterable<Publication> getPublications(){
        return publicationRepository.findAll();
    }

    @PostMapping("/publications")
    public String rootSave(@RequestParam("text") String text,
                           @RequestParam("image") MultipartFile image) throws IOException {
       return publicationService.addPost(text, image);
    }
    @GetMapping(value = "/images/{name}", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getImage(@PathVariable("name") String name) {
        String path = "../images";
        try {
            InputStream is = new FileInputStream(new File(path)+"/"+name);
            return StreamUtils.copyToByteArray(is);
        } catch (Exception e) {
            InputStream is = null;
            try {
                is = new FileInputStream(new File(path) + "/" +name);
                return StreamUtils.copyToByteArray(is);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }
//    @PostMapping("/comment")
//    public String addComment(Authentication authentication, @RequestBody CommentDTO commentData){
//        User user = (User) authentication.getPrincipal();
//        return publicationService.addComment(commentData, user.getId());
//    }
//    @DeleteMapping("comment/{commentId}")
//    public ResponseEntity<Void> deleteComment(Authentication authentication, @PathVariable String commentId) {
//        User user = (User) authentication.getPrincipal();
//        if (publicationService.deleteComment(user.getId(), commentId))
//            return ResponseEntity.noContent().build();
//        return ResponseEntity.notFound().build();
//    }
//
//    @DeleteMapping("{userId}")
//    public ResponseEntity<Void> deleteUser(Authentication authentication) {
//        User user = (User) authentication.getPrincipal();
//        if (userService.deleteUser(user.getId()))
//            return ResponseEntity.noContent().build();
//        return ResponseEntity.notFound().build();
//    }
//
//    @DeleteMapping("/publication/{publicationId}")
//    public ResponseEntity<Void> deletePublication(Authentication authentication, @PathVariable String publicationId) {
//        User user = (User) authentication.getPrincipal();
//        if (publicationService.deletePublication(publicationId, user.getId()))
//            return ResponseEntity.noContent().build();
//        return ResponseEntity.notFound().build();
//    }
//    @PostMapping("/subscribe")
//    public SubscribesDTO subscribeOnUser(Authentication authentication, @RequestBody SubscribesDTO subscriptionData){
//        User user = (User) authentication.getPrincipal();
//        return subscriptionService.createSubscription(user.getId(), subscriptionData);
//    }
//    @PostMapping("/publication/like")
//    public String likePublication(Authentication authentication, @RequestBody LikeDTO likeData) {
//         User user = (User) authentication.getPrincipal();
//         return publicationService.likePublication(user.getId(), likeData);
//    }
}
