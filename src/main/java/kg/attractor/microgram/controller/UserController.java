package kg.attractor.microgram.controller;

import kg.attractor.microgram.dto.PublicationDTO;
import kg.attractor.microgram.dto.UserDTO;
import kg.attractor.microgram.model.Comment;
import kg.attractor.microgram.repository.CommentRepository;
import kg.attractor.microgram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("*")
public class UserController {
    @Autowired
    CommentRepository commentRepository;

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping("/getComments/{publicationId}")
    public Iterable<Comment> getComments(@PathVariable("publicationId") String publicationId){
        try {
            return commentRepository.getCommentsByPublicationId(publicationId);
        }catch (Exception e){

        }
        return null;
    }

   @PostMapping("/register")
   public UserDTO registerUser(@RequestBody UserDTO userData){
        return userService.addUser(userData);
   }
    @GetMapping("/profile/{userId}")
    public List<PublicationDTO> getProfile(@PathVariable String userId){
        return userService.getPublications(userId);
    }

}