package kg.attractor.microgram.service;

import kg.attractor.microgram.model.Comment;
import kg.attractor.microgram.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;

    public String createComment(String user_id, String post_id, String comment){
        Comment newComment = new Comment(user_id, post_id, comment);
        commentRepository.save(newComment);
        return "success";
    }
}
