package kg.attractor.microgram.service;

import kg.attractor.microgram.dto.PublicationDTO;
import kg.attractor.microgram.dto.UserDTO;
import kg.attractor.microgram.model.Publication;
import kg.attractor.microgram.model.User;
import kg.attractor.microgram.repository.CommentRepository;
import kg.attractor.microgram.repository.PublicationRepository;
import kg.attractor.microgram.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;
    private final PublicationRepository publicationRepository;
    private final CommentRepository commentRepository;
    private final PasswordEncoder encoder;


    public UserDTO addUser(UserDTO userData){
        User user = User.builder()
                .id(userData.getId())
                .email(userData.getEmail())
                .name(userData.getName())
                .password(encoder.encode(userData.getPassword()))
                .build();
        userRepository.save(user);
        return UserDTO.from(user);
    }
    public List<PublicationDTO> getPublications(String userId){
        List<Publication> publications = publicationRepository.findAllByUserId(userId);
        List<PublicationDTO> publicationDTO = new ArrayList<>();
        for(Publication p: publications){
            PublicationDTO pDTO = PublicationDTO.from(p);
            publicationDTO.add(pDTO);
        }
        return publicationDTO;
    }
    public boolean deleteUser(String userId){
        userRepository.deleteById(userId);
        return true;
    }
}
