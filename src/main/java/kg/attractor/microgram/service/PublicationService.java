package kg.attractor.microgram.service;

import kg.attractor.microgram.dto.CommentDTO;
import kg.attractor.microgram.dto.LikeDTO;
import kg.attractor.microgram.dto.PublicationDTO;
import kg.attractor.microgram.exception.ResourceNotFoundException;
import kg.attractor.microgram.model.Comment;
import kg.attractor.microgram.model.Like;
import kg.attractor.microgram.model.Publication;
import kg.attractor.microgram.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;

@Service
public class PublicationService {
    private final UserRepository userRepository;
    private final PublicationRepository publicationRepository;
    private final CommentRepository commentRepository;
    private final LikeRepository likeRepository;

    public PublicationService(UserRepository userRepository, PublicationRepository publicationRepository, CommentRepository commentRepository, LikeRepository likeRepository) {
        this.userRepository = userRepository;
        this.publicationRepository = publicationRepository;
        this.commentRepository = commentRepository;
        this.likeRepository = likeRepository;
    }
    public String addPost(String text, MultipartFile image) throws IOException {
        String path = "../images";
        File imageFile = new File(path + "/" + image.getOriginalFilename());
        FileOutputStream os = new FileOutputStream(imageFile);
        os.write(image.getBytes());
        os.close();

        Publication publication = new Publication(text, image.getOriginalFilename());
        publicationRepository.save(publication);
        return "success";

    }
    public boolean deletePublication(String publicationId, String userId) {
        if(publicationRepository.findById(publicationId).get().getUserId().equals(userId)) {
            publicationRepository.deleteById(publicationId);
            commentRepository.deleteAll(commentRepository.findAllByPublicationId(publicationId));
            likeRepository.deleteAll(likeRepository.findAllByPublicationId(publicationId));
            return true;
        }
        return false;
    }
    public String addComment(CommentDTO commentData, String userId){
        if(publicationRepository.existsById(commentData.getPublicationId())){
            var comment = Comment.builder()
                    .id(commentData.getId())
                    .userId(userId)
                    .publicationId(commentData.getPublicationId())
                    .comment(commentData.getComment())
                    .commentTime(LocalDateTime.now())
                    .build();
            commentRepository.save(comment);
            return String.format("id: %s\n publicationId: %s\n, text: %s\n, time: %s",
                    comment.getId(), comment.getPublicationId(), comment.getComment(), comment.getCommentTime());
        }
        return "This publication doen't exist";
    }
    public boolean deleteComment(String userId, String commentId) {
       if(commentRepository.findById(commentId).get().getUserId().equals(userId)){
           commentRepository.delete(commentRepository.findById(commentId).get());
           return true;
       }
       return false;
    }
    public String likePublication(String userId, LikeDTO likeData){
    if(publicationRepository.existsById(likeData.getPublicationId())){
        var like = Like.builder()
                .id(likeData.getId())
                .userId(userId)
                .publicationId(likeData.getPublicationId())
                .likeTime(LocalDateTime.now())
                .build();
       likeRepository.save(like);
        return String.format("id: %s\n publicationId: %s\n, time: %s",
               like.getId(), like.getPublicationId(), like.getLikeTime());
        }
        return "Publication with this Id does not exist";
    }
}
