package kg.attractor.microgram.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection = "comments")
public class Comment {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    @Indexed
    private String userId;
    private String publicationId;
    private String comment;
    private LocalDateTime commentTime;

    public Comment(String userId, String publicationId, String comment) {
        this.userId = userId;
        this.publicationId = publicationId;
        this.comment = comment;
        this.commentTime = LocalDateTime.now();
    }
}
