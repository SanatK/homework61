package kg.attractor.microgram.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="publications")
public class Publication {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String image;
    private String description;
    private LocalDateTime publicationTime;
    @Builder.Default
    private String userId = UUID.randomUUID().toString().substring(1, 7);

    public Publication(String description, String image){
        this.description = description;
        this.image = image;
        this.publicationTime = LocalDateTime.now();
    }
}
