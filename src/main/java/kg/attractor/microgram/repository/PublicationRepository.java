package kg.attractor.microgram.repository;

import kg.attractor.microgram.model.Publication;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PublicationRepository extends CrudRepository<Publication, String> {
    public void deleteById(String id);
    List<Publication> findAllByUserId(String userId);
}
