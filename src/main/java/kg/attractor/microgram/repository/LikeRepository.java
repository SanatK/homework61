package kg.attractor.microgram.repository;

import kg.attractor.microgram.model.Like;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LikeRepository extends CrudRepository<Like, String> {
    List<Like> findAllByPublicationId(String publicationId);
}
