package kg.attractor.microgram.repository;

import kg.attractor.microgram.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    public User findByName(String name);
    Optional<User> getByEmail(String email);
}
