package kg.attractor.microgram.repository;

import kg.attractor.microgram.model.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, String> {
    List<Comment> findAllByPublicationId(String publicationId);
    List<Comment> findAllByUserId(String userId);
    Iterable<Comment> getCommentsByPublicationId(String publicationId);

}
