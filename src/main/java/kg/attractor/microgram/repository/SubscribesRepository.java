package kg.attractor.microgram.repository;

import kg.attractor.microgram.model.Subscribes;
import org.springframework.data.repository.CrudRepository;

public interface SubscribesRepository extends CrudRepository<Subscribes, String> {
}
